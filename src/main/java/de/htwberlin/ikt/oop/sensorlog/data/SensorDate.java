package de.htwberlin.ikt.oop.sensorlog.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SensorDate
{
    private Calendar date;

    public SensorDate(int year, int month, int day) {
        this.date = new GregorianCalendar(year, month, day);
    }

    @Override
    public String toString() {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        return format.format(date.getTime());
    }
}
