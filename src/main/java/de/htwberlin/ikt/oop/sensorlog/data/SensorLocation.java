/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.htwberlin.ikt.oop.sensorlog.data;

/**
 * Location of a sensor.
 *
 * @author Kay Otto
 */
public enum SensorLocation
{
    //current locations
    ROME("Rom", "Italien", "it"),
    PARIS("Paris", "Frankreich", "fr"),
    BERLIN("Berlin", "Deutschland", "de"),
    NEWYORK("New York", "Amerika", "us"),
    WARSAW("Warschau", "Polen", "pl"),
    NEWHAGEN("Neuenhagen", "Deutschland", "de"),
    PTOWN("Petershagen", "Polen", "pl"),
    MOSCOW("Moskau", "Russland", "ru");

    /**
     * the city name
     */
    public final String cityName;

    /**
     * the country the city is located in
     */
    public final String countryName;

    /**
     * the ISO 3166 country code
     */
    public final String countryCode;

    /**
     * Constructs a SensorLocation constant.
     *
     * @param cityName    the name of the city
     * @param countryName the country the city is located in
     * @param countryCode the ISO 3166 country code
     * @throws IllegalArgumentException if one of the parameters is null or an empty string
     */
    SensorLocation(String cityName, String countryName, String countryCode)
    {
        if (cityName != null && !cityName.isEmpty()) {
            this.cityName = cityName;
        } else {
            throw new IllegalArgumentException("CityName must not be null or empty.");
        }

        if (countryName != null && !countryName.isEmpty()) {
            this.countryName = countryName;
        } else {
            throw new IllegalArgumentException("CountryName must not be null or empty.");
        }

        if (countryCode != null && !countryCode.isEmpty()) {
            this.countryCode = countryCode;
        } else {
            throw new IllegalArgumentException("CountyCode must not be null or empty.");
        }
    }

    @Override
    public String toString()
    {
        return this.cityName + " (" + this.countryName + ")";
    }
}
