/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.htwberlin.ikt.oop.sensorlog.data;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Sensor log containing sensor values obtained from different sensors.
 *
 * @author Kay Otto
 */
public class SensorLog
{
    private final ArrayList<SensorValue> sensorValues = new ArrayList<>();

    /**
     * Add sensor value to the log.
     *
     * @param value the sensor value
     * @throws IllegalArgumentException if the sensor value is null
     */
    public void addSensorValue(SensorValue value)
    {
        if (value != null) {
            sensorValues.add(value);

            //for debugging reasons, print out all sensor values to console
            System.out.println(this.getAllValuesAsFormattedString(true, true));
        } else {
            throw new IllegalArgumentException("Value must not be null.");
        }
    }

    /**
     * Remove sensor value from the log.
     *
     * @param index the index of the sensor value to be removed
     * @throws IndexOutOfBoundsException if the specified index is out of range
     */
    public void removeSensorValue(int index)
    {
        this.sensorValues.remove(index);
    }

    /**
     * Removes all the sensor values from the log.
     */
    public void clearSensorValues()
    {
        sensorValues.clear();
    }

    /**
     * Get the sensor value at the specified index.
     *
     * @param index The index of the sensor value within the list
     * @return the sensor value
     * @throws IndexOutOfBoundsException if the specified index is out of range
     */
    public SensorValue getSensorValue(int index)
    {
        return sensorValues.get(index);
    }

    /**
     * Gets all sensor values from the log that have the specified location.
     *
     * @param location the location
     * @return the sensor values
     */
    public Collection<SensorValue> getSensorValues(SensorLocation location)
    {
        ArrayList<SensorValue> result = new ArrayList<>();
        for (SensorValue v : this.sensorValues) {
            if (v.getLocation() == location) {
                result.add(v);
            }
        }

        return result;
    }

    /**
     * Gets all sensor values for a given sensor type specified by its class.
     *
     * @param sensorValueType the class of the sensor type
     * @return sensor values of the specified type
     */
    public Collection<SensorValue> getSensorValues(Class sensorValueType)
    {
        ArrayList<SensorValue> result = new ArrayList<>();
        for (SensorValue v : this.sensorValues) {
            if (sensorValueType.isAssignableFrom(v.getClass())) {
                result.add(v);
            }
        }

        return result;
    }

    /**
     * Gets the number of sensor values currently stored in the log.
     *
     * @return the number of sensor values
     */
    public int getNumSensorValues()
    {
        return sensorValues.size();
    }

    /**
     * Gets a string containing a formatted output of all sensor values currently stored in the log.
     *
     * @param withTimeStamp if false, dismiss timestamps in formatted string
     * @param withLocation  if false, dismiss locations in formatted string
     * @return the formatted string
     */
    public String getAllValuesAsFormattedString(boolean withTimeStamp, boolean withLocation)
    {
        StringBuilder sb = new StringBuilder();

        for (SensorValue value : sensorValues) {
            sb.append(value.getAsFormattedString(withTimeStamp, withLocation));
            sb.append("\n");
        }

        return sb.toString();
    }
}
