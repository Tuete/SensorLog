package de.htwberlin.ikt.oop.sensorlog.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SensorTime
{
    private Calendar time;

    public SensorTime(int hour, int minute, int second) {
        this.time = new GregorianCalendar();
        this.time.set(Calendar.HOUR_OF_DAY, hour);
        this.time.set(Calendar.MINUTE, minute);
        this.time.set(Calendar.SECOND, second);
    }

    @Override
    public String toString() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        return format.format(time.getTime());
    }
}
