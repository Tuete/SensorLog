/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.htwberlin.ikt.oop.sensorlog.data;

/**
 * @author kay
 */
public class SensorTimeStamp
{
    public final SensorDate date;
    public final SensorTime time;

    public SensorTimeStamp(SensorDate date, SensorTime time)
    {
        if (date != null) {
            this.date = date;
        } else {
            throw new IllegalArgumentException("Date must not be null.");
        }

        if (time != null) {
            this.time = time;
        } else {
            throw new IllegalArgumentException("Time must not be null.");
        }
    }

    @Override
    public String toString()
    {
        return date.toString() + " um " + time.toString();
    }

}
