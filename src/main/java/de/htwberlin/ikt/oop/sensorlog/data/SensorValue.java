package de.htwberlin.ikt.oop.sensorlog.data;

/**
 * Abstract base class for different sensor values
 *
 * @author Kay Otto
 */
public abstract class SensorValue
{
    private SensorLocation location;
    private SensorTimeStamp timeStamp;
    private double value;

    /**
     * Constructs a new object of this type.
     *
     * @param location  the location of the sensor value
     * @param timeStamp the timestamp of the sensor value
     * @param value     the sensor value
     * @throws IllegalArgumentException if one of the parameters is null or out of range
     */
    public SensorValue(SensorLocation location, SensorTimeStamp timeStamp, double value)
    {
        this.setLocation(location);
        this.setTimeStamp(timeStamp);
        this.setValue(value);
    }

    /**
     * Gets the sensor value converted to a string representation.
     *
     * @return the sensor value
     */
    public abstract String getValueAsString();

    /**
     * Returns the unit of the sensor value, e.g. °C
     *
     * @return the unit
     */
    public abstract String getUnit();

    /**
     * Returns the maxium value allowed for the sensor value.
     *
     * @return the maximum value allowed
     */
    public abstract double getMaxValue();

    /**
     * Returns the minimum value allowed for the sensor value.
     *
     * @return the minimum value allowed
     */
    public abstract double getMinValue();

    /**
     * Returns the measured quantity the sensor provides, e.g. Temperature.
     *
     * @return the measured quantity
     */
    public abstract String getSensorTitle();


    /**
     * Sets the sensor value.
     *
     * @param value the sensor value
     * @throws IllegalArgumentException if the specified value is out of its allowed range
     */
    public final void setValue(double value)
    {
        if (value < this.getMinValue() || value > this.getMaxValue()) {
            throw new IllegalArgumentException("Value has to be in range [" + this.getMinValue() + ".." + this.getMaxValue() + "].");
        }
        this.value = value;
    }

    /**
     * Returns the sensor value
     *
     * @return the value
     */
    public final double getValue()
    {
        return this.value;
    }

    /**
     * Gets the sensor value as formatted string, e.g. "Temperatur am 20.12.2016 um 22:05h in Berlin, Deutschland: 23.6 °C
     *
     * @param withTimeStamp if false, dismiss timestamp in formatted string
     * @param withLocation  if false, dismiss location in formatted string
     * @return the formatted string
     */
    public String getAsFormattedString(boolean withTimeStamp, boolean withLocation)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getSensorTitle());

        if (withTimeStamp) {
            sb.append(" am ");
            sb.append(timeStamp.toString());
        }
        if (withLocation) {
            sb.append(" in ");
            sb.append(location.toString());
        }

        sb.append(": ");
        sb.append(this.getValueAsString());
        sb.append(" ");
        sb.append(this.getUnit());

        return sb.toString();
    }


    /**
     * Sets the location of the sensor value
     *
     * @param location the location
     * @throws IllegalArgumentException if the specified location is null
     */
    public final void setLocation(SensorLocation location)
    {
        if (location != null) {
            this.location = location;
        } else {
            throw new IllegalArgumentException("Location must not be null.");
        }
    }

    /**
     * Returns the location of the sensor value
     *
     * @return the location
     */
    public final SensorLocation getLocation()
    {
        return location;
    }

    /**
     * Sets the timestamp of the sensor value
     *
     * @param timeStamp the timestamp
     * @throws IllegalArgumentException if the specified timestamp is null
     */
    public final void setTimeStamp(SensorTimeStamp timeStamp)
    {
        if (timeStamp != null) {
            this.timeStamp = timeStamp;
        } else {
            throw new IllegalArgumentException("Timestamp must not be null.");
        }
    }

    /**
     * Returns the timestamp of the sensor value
     *
     * @return the timestamp
     */
    public final SensorTimeStamp getTimeStamp()
    {
        return timeStamp;
    }
}
