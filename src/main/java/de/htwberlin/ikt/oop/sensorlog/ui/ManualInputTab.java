/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.htwberlin.ikt.oop.sensorlog.ui;

import com.sun.javafx.collections.ObservableListWrapper;
import de.htwberlin.ikt.oop.sensorlog.data.*;
import de.htwberlin.ikt.oop.sensorlog.values.AirPressureValue;
import de.htwberlin.ikt.oop.sensorlog.values.HumidityValue;
import de.htwberlin.ikt.oop.sensorlog.values.TemperatureValue;
import de.htwberlin.ikt.oop.sensorlog.values.WindSpeedValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Tab for manual input of sensor values.
 *
 * @author Kay Otto
 */
public class ManualInputTab extends SensorLogAwareTab
{
    private final ComboBox<SensorLocation> locationBox = new ComboBox<>();
    private final ComboBox<String> sensorTitleBox = new ComboBox<>();
    private final TextField valueTextField = new TextField();
    private final Label statusLabelNumTotalSensorValues = new Label();
    private final Label statusLabelNumTemperatureSensorValues = new Label();
    private final Label statusLabelNumAirPressureSensorValues = new Label();
    private final Label statusLabelNumHumiditySensorValues = new Label();
    private final Label statusLabelNumWindSpeedSensorValues = new Label();

    public ManualInputTab(SensorLog sensorLog)
    {
        super(sensorLog);

        VBox root = new VBox(VisualDefaults.SPACING);
        root.setPadding(new Insets(VisualDefaults.SPACING));

        this.addLocationComboBox(root);
        this.addSensorComboBox(root);

        /**
         * Add textfield for the input value.
         */
        valueTextField.setPromptText("Hier den Sensorwert eingeben");
        root.getChildren().add(createItem("Sensorwert", valueTextField));

        this.addButtonForAddingSensorValue(root);
        this.addStatusLabels(root);
        this.addButtonForRemovingAllSensorValues(root);

        this.setContent(root);
        this.setClosable(false);
        this.setText("Manuelle Eingabe");
        this.refresh();
    }

    private static HBox createItem(String title, Control control)
    {
        control.setPrefWidth(200);
        HBox hBox = new HBox(VisualDefaults.SPACING);
        Label label = new Label(title);
        label.setPrefWidth(150);
        hBox.getChildren().add(label);
        hBox.getChildren().add(control);
        hBox.setAlignment(Pos.CENTER_LEFT);

        return hBox;
    }

    private static SensorTimeStamp getCurrentTimeStamp()
    {
        Calendar calendar = new GregorianCalendar();
        return new SensorTimeStamp(
                new SensorDate(
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH) + 1,
                        calendar.get(Calendar.DAY_OF_MONTH)),
                new SensorTime(
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        calendar.get(Calendar.SECOND)));
    }

    private void addLocationComboBox(Pane container)
    {
        ArrayList<SensorLocation> locationList = new ArrayList<>(Arrays.asList(SensorLocation.values()));

        locationBox.setItems(new ObservableListWrapper(locationList));
        locationBox.getSelectionModel().selectFirst();

        container.getChildren().add(createItem("Ort", locationBox));
    }

    private void addSensorComboBox(Pane container)
    {
        ArrayList<String> sensorTitleList = new ArrayList<>();
        sensorTitleList.add("Temperatur [°C]");
        sensorTitleList.add("Luftdruck [hPa]");
        sensorTitleList.add("Luftfeuchtigkeit [%]");
        sensorTitleList.add("Windgeschwindigkeit [m/s]");

        sensorTitleBox.setItems(new ObservableListWrapper(sensorTitleList));
        sensorTitleBox.getSelectionModel().selectFirst();

        container.getChildren().add(createItem("Sensortyp", sensorTitleBox));
    }

    private void addButtonForAddingSensorValue(Pane container)
    {
        Button button = new Button("Wert dem Logbuch hinzufügen");
        button.setPrefWidth(354);
        button.setOnAction(event -> {
            String input = valueTextField.getText();
            if (input.isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Bitte spezifizieren Sie zunächst einen Sensorwert.", ButtonType.OK);
                alert.show();
                valueTextField.requestFocus();
            } else {
                try {
                    double value = Double.parseDouble(valueTextField.getText());
                    SensorLocation location = locationBox.getSelectionModel().getSelectedItem();
                    SensorTimeStamp timeStamp = getCurrentTimeStamp();
                    Alert alert = new Alert(
                            Alert.AlertType.ERROR,
                            "Der spezifizierte Wert liegt außerhalb des erlaubten Bereichs.", ButtonType.OK);

                    try {
                        switch (sensorTitleBox.getSelectionModel().getSelectedIndex()) {
                            case 0:
                                getSensorLog().addSensorValue(new TemperatureValue(location, timeStamp, value));
                                break;
                            case 1:
                                getSensorLog().addSensorValue(new AirPressureValue(location, timeStamp, value));
                                break;
                            case 2:
                                getSensorLog().addSensorValue(new HumidityValue(location, timeStamp, value));
                                break;
                            case 3:
                                getSensorLog().addSensorValue(new WindSpeedValue(location, timeStamp, value));
                                break;
                        }
                        refresh();
                    } catch (IllegalArgumentException e) {
                        alert.show();
                        valueTextField.requestFocus();
                    }
                } catch (NumberFormatException ex) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "Bitte spezifizieren Sie einen numerischen Sensorwert.", ButtonType.OK);
                    alert.show();
                    valueTextField.setText("");
                    valueTextField.requestFocus();
                }
            }
        });
        container.getChildren().add(button);
    }

    @Override
    public void refresh()
    {
        statusLabelNumTotalSensorValues.setText(getSensorLog().getNumSensorValues() + "\t\tSensorwerte insgesamt");
        statusLabelNumTemperatureSensorValues.setText(getSensorLog().getSensorValues(TemperatureValue.class).size() + "\t\tSensorwerte für Temperatur");
        statusLabelNumAirPressureSensorValues.setText(getSensorLog().getSensorValues(AirPressureValue.class).size() + "\t\tSensorwerte für Luftdruck");
        statusLabelNumHumiditySensorValues.setText(getSensorLog().getSensorValues(HumidityValue.class).size() + "\t\tSensorwerte für Luftfeuchtigkeit");
        statusLabelNumWindSpeedSensorValues.setText(getSensorLog().getSensorValues(WindSpeedValue.class).size() + "\t\tSensorwerte für Windgeschwindigkeit");
    }

    private void addButtonForRemovingAllSensorValues(Pane container)
    {
        Button button = new Button("Alle Werte aus Logbuch löschen");
        button.setPrefWidth(354);
        button.setOnAction(event -> getSensorLog().clearSensorValues());
        container.getChildren().add(button);
    }

    private void addStatusLabels(Pane container)
    {
        container.getChildren().add(statusLabelNumTotalSensorValues);
        container.getChildren().add(statusLabelNumTemperatureSensorValues);
        container.getChildren().add(statusLabelNumAirPressureSensorValues);
        container.getChildren().add(statusLabelNumHumiditySensorValues);
        container.getChildren().add(statusLabelNumWindSpeedSensorValues);
    }
}
