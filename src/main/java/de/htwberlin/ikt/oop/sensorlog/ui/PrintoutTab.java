/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.htwberlin.ikt.oop.sensorlog.ui;

import de.htwberlin.ikt.oop.sensorlog.data.SensorLog;
import de.htwberlin.ikt.oop.sensorlog.data.SensorValue;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

/**
 * Tab for printout of all sensor values in log.
 *
 * @author Kay Otto
 */
public class PrintoutTab extends SensorLogAwareTab
{
    private VBox root;

    public PrintoutTab(SensorLog sensorLog)
    {
        super(sensorLog);

        ScrollPane scrollPane = new ScrollPane();

        root = new VBox(VisualDefaults.SPACING);
        scrollPane.setContent(root);
        root.setPadding(new Insets(VisualDefaults.SPACING));

        this.setContent(scrollPane);
        this.setClosable(false);
        this.setText("Ausdruck aller Sensorwerte");
        this.refresh();
    }

    @Override
    public void refresh()
    {
        root.getChildren().clear();

        SensorValue sensorValue;
        for (int i = 0; i < this.getSensorLog().getNumSensorValues(); i++) {
            sensorValue = this.getSensorLog().getSensorValue(i);
            addSensorValueRepresentation(root, sensorValue);
        }
    }

    private static void addSensorValueRepresentation(Pane container, SensorValue sensorValue)
    {
        VBox vBox = new VBox(VisualDefaults.SPACING);
        vBox.setPadding(new Insets(VisualDefaults.SPACING));

        Label label = new Label(sensorValue.getAsFormattedString(true, true));
        vBox.getChildren().add(label);

        Color color = VisualDefaults.COLORMAP.get(sensorValue.getClass());
        if (color != null) {
            vBox.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
        }

        container.getChildren().add(vBox);
    }
}
