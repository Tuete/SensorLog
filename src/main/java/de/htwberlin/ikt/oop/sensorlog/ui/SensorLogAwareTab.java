/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.htwberlin.ikt.oop.sensorlog.ui;

import de.htwberlin.ikt.oop.sensorlog.data.SensorLog;
import javafx.scene.control.Tab;

/**
 * An abstract base class for a Tab that knows a SensorLog object
 *
 * @author Kay Otto
 */
public abstract class SensorLogAwareTab extends Tab
{
    //the SensorLog object
    private SensorLog sensorLog;

    /**
     * Creates a Tab that knows a SensorLog object
     *
     * @param sensorLog the SensorLog object
     */
    public SensorLogAwareTab(SensorLog sensorLog)
    {
        this.setSensorLog(sensorLog);
    }

    /**
     * Gets the SensorLog object
     *
     * @return the SensorLog object
     */
    public final SensorLog getSensorLog()
    {
        return sensorLog;
    }

    /**
     * Sets the SensorLog object
     *
     * @param sensorLog the SensorLog object
     */
    public final void setSensorLog(SensorLog sensorLog)
    {
        if (sensorLog != null) {
            this.sensorLog = sensorLog;
        } else {
            throw new IllegalArgumentException("SensorLog must not be null.");
        }
    }

    /**
     * Refresh the contents if needed. Will be called if the tab is selected.
     */
    public abstract void refresh();
}
