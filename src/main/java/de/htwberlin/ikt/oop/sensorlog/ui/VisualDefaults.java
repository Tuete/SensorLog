/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.htwberlin.ikt.oop.sensorlog.ui;

import de.htwberlin.ikt.oop.sensorlog.values.AirPressureValue;
import de.htwberlin.ikt.oop.sensorlog.values.HumidityValue;
import de.htwberlin.ikt.oop.sensorlog.values.TemperatureValue;
import de.htwberlin.ikt.oop.sensorlog.values.WindSpeedValue;
import javafx.scene.paint.Color;

import java.util.HashMap;

/**
 * Default metrics and colors for visual appearance.
 *
 * @author Kay Otto
 */
public class VisualDefaults
{
    //default spacing and padding of visual controls
    public static final double SPACING = 4;

    //map for mapping a special type of sensor value to a color for printout
    public final static HashMap<Class, Color> COLORMAP;

    static {
        COLORMAP = new HashMap<>();
        COLORMAP.put(TemperatureValue.class, Color.LIGHTGREY);
        COLORMAP.put(AirPressureValue.class, Color.LIGHTGREEN);
        COLORMAP.put(HumidityValue.class, Color.LIGHTBLUE);
        COLORMAP.put(WindSpeedValue.class, Color.LIGHTYELLOW);
    }
}
