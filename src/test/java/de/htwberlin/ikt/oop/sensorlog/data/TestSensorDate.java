package de.htwberlin.ikt.oop.sensorlog.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestSensorDate
{
    @Test
    void TestToString()
    {
        SensorDate sensorDate = new SensorDate(2017, 11, 17);
        Assertions.assertEquals("17.12.2017", sensorDate.toString());
    }
}
