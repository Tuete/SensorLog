package de.htwberlin.ikt.oop.sensorlog.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSensorTime
{
    @Test
    void TestToString()
    {
        SensorTime sensorTime = new SensorTime(14, 20, 0);
        Assertions.assertEquals("14:20:00", sensorTime.toString());
    }
}
